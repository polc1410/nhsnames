# Changelog

## 0.0.0.9001
* Documentation improvements on website to include installation instructions.
* Formatting improvements on website.

## 0.0.0.9000
Initial release
